<?php

/**
 * Copyright (c) 2017, 2022 Raphaël . Droz + floss @ gmail DOT com
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 */

namespace WP_CLI\WpImageLabel;

if (! defined('ABSPATH')) {
    exit;
}

require __DIR__ . '/../vendor/autoload.php';

use Google\Cloud\Vision\VisionClient;
use Google\Cloud\Vision\Image;

class Tagger
{
    // https://stackoverflow.com/a/8159439
    public static function retrieve_remote_file_size($url)
    {
        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_NOBODY, true);

        $data = curl_exec($ch);
        $size = curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);

        curl_close($ch);
        return $size;
    }

    // export GOOGLE_APPLICATION_CREDENTIALS=auth.json
    public static function google_detect_label($image_sources)
    {
        if (! $image_sources) {
            return [];
        }

        $vision = new VisionClient();
        $images = $vision->images($image_sources, ['LABEL_DETECTION']);
        $responses = $vision->annotateBatch($images);

        $i = 0;
        $ret = [];
        foreach ($responses as $annotation) {
            $error = $annotation->error();
            if ($error) {
                // error 3 - Invalid language hints: probably a partially uploaded jpeg image
                fprintf(STDERR, "# LABELS for %s : error %s - %s" . PHP_EOL, $image_sources[$i], $error['code'], $error['message']);
                $ret[$image_sources[$i]] = ['error' . $error['code']];
            } else {
                $labels = $annotation->labels();
                $ret[$image_sources[$i]] = [];
                fprintf(STDERR, "# LABELS for %s" . PHP_EOL, $image_sources[$i]);
                foreach ($labels as $label) {
                    fprintf(STDERR, "\t %s %s" . PHP_EOL, $label->description(), $label->score());
                    $ret[$image_sources[$i]][$label->description()] = $label->score();
                }
            }
            $i++;
        }
        return $ret;
    }

    public static function get_attachments_missing_tags($time, $check = true)
    {
        $attachments = get_posts([ 'post_type' => 'attachment',
                                   'post_status' => 'inherit',
                                   // https://cloud.google.com/vision/limits
                                   'posts_per_page' => 16,
                                   'date_query' => [
                                       [ 'after' => $time ]
                                   ],
                                   'meta_query' => [
                                       [ 'key' => 'aitags', 'compare' => 'NOT EXISTS' ],
                                       [ 'key' => '_wp_attached_file', 'value' => 'comments/', 'compare' => 'LIKE' ]
                                   ] ]);


        $ret = [];
        foreach ($attachments as $attachment) {
            $public_image = wp_get_attachment_url($attachment->ID);
            // $meta = wp_get_attachment_metadata($attachment->ID);
            if ($check) {
                $filesize = self::retrieve_remote_file_size($public_image);
                // Google's 10MB limit
                if ($filesize < 1024 || $filesize > 10 * 1024 ** 2) {
                    fprintf(STDERR, "ignored file %s (size %d)\n", $public_image, $filesize);
                    update_post_meta($attachment->ID, "aitags", false);
                    continue;
                }
            }
            $ret[$public_image] = [ $attachment->ID, $attachment, $filesize ];
        }
        return $ret;
    }

    public static function tag_attachments($time, $opts)
    {
        $urls = self::get_attachments_missing_tags($time, $opts['check']);
        if ($opts['verbose']) {
            array_walk($urls, function ($v, $k) {
                fprintf(STDERR, "%s (%d bytes)\n", $k, $v[2]);
            });
        }

        if (! $opts['dry-run']) {
            $tags = Tagger::google_detect_label(array_keys($urls));
            array_walk($tags, function ($arr, $source) use ($urls) {
                printf(
                    "image %s (%d bytes), tags: %s\n",
                    $source,
                    $urls[$source][2],
                    implode(',', array_keys($arr))
                );
                if (count($arr) > 0) {
                    update_post_meta($urls[$source][0], "aitags", $arr);
                    update_post_meta($urls[$source][0], "aitagstime", time());
                }
            });
        }
    }

    public static function dump_labels($limit)
    {
        $attachments = get_posts([ 'post_type' => 'attachment',
                                    'post_status' => 'inherit',
                                    'posts_per_page' => (int)$limit,
                                    'meta_query' => [
                                        'relation' => 'AND',
                                        [ 'key' => 'aitags', 'compare' => 'EXISTS' ],
                                        [ 'key' => 'aitags', 'value' => '.+', 'compare' => 'REGEXP' ]
                                    ] ]);

        array_walk($attachments, function ($attachment) {
            $labels = get_post_meta($attachment->ID, "aitags")[0];
            if ($labels == 1) {
                $labels = array_flip(explode(',', get_post_meta($attachment->ID, "_wp_attachment_image_alt")[0]));
            }
            $labeltime = get_post_meta($attachment->ID, "aitagstime")[0];
            printf(
                "id:% 5d, url:%s, labelled %s\n\ttags: %s\n",
                $attachment->ID,
                wp_get_attachment_url($attachment->ID),
                strftime("%F %T", $labeltime),
                implode(',', array_keys($labels))
            );
        });
    }

    public static function alert_strange_labels($time, $callback, $opts = [])
    {
        if (! $time || $time < 0 || $time > 5 * YEAR_IN_SECONDS) {
            $time = 50 * HOUR_IN_SECONDS;
        }
        $attachments = get_posts([ 'post_type' => 'attachment',
                                    'post_status' => 'inherit',
                                    'meta_query' => [
                                        'relation' => 'AND',
                                        [ 'key' => 'aitagstime', 'value' => time() - $time, 'compare' => '>=' ],
                                        [ 'key' => 'aitagsalerted', 'compare' => 'NOT EXISTS' ]
                                    ] ]);

        $alert = 'These images do not appears to validate :';
        $ret = [];
        foreach ($attachments as $attachment) {
            $labels = get_post_meta($attachment->ID, "aitags")[0];
            $labeltime = get_post_meta($attachment->ID, "aitagstime")[0];
            if ($callback($labels)) {
                $alert_line = sprintf(
                    "id:% 5d, url:%s, labelled %s\n\ttags: %s\n",
                    $attachment->ID,
                    wp_get_attachment_url($attachment->ID),
                    strftime("%F %T", $labeltime),
                    implode(',', array_keys($labels))
                );

                if (isset($opts['parent_resource_callback'])) {
                    $c = $opts['parent_resource_callback']($attachment->ID);
                    if ($c) {
                        $alert_line .= "\t" . implode(', ', $c) . "\n";
                    }
                }
                if (isset($opts['verbose'])) {
                    printf($alert_line . PHP_EOL);
                }
                if (isset($opts['email'])) {
                    $alert .= $alert_line . "\r\n";
                }
                $ret[] = $attachment;
            }
        }
        if ($ret && isset($opts['email'])) {
            if (
                wp_mail(
                    $opts['email'],
                    sprintf('The labels of %d image(s) failed to match', count($ret)),
                    $alert
                )
            ) {
                foreach ($ret as $r) {
                    update_post_meta($r->ID, 'aitagsalerted', time());
                }
            }
        }
    }
}
