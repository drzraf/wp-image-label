<?php

/**
 * Label WP attachment using Google Vision API.
 *
 * @package drzraf/wp-image-label
 *
 * Copyright (c) 2017, 2022 Raphaël . Droz + floss @ gmail DOT com
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 */

namespace WP_CLI\WpImageLabel;

use WP_CLI;

if (! getenv('GOOGLE_APPLICATION_CREDENTIALS')) {
    return;
}

/**
 * Provides several subcommands related to labeling WordPress medias using Google IA Vision API.
 *
 * It can submit batches of unlabeled medias to the API, store the result and provide command
 * to deal with them or store labels in a dedicated field or as a standard media alt-field and
 * proceed both inline (direct database access) or using flat/CSV/JSON lists of URL and labels.
 *
 * ## EXAMPLE SCENARIO
 *
 * $ wp media label candidates --root=https://foo.org/app/ --missing-alt > urls.csv
 * Extract attachments (id, guid and alt) from the database and store them as a flat CSV file
 *
 * $ awk '{print $2}' urls.csv | wp media label fetch --filter=filter.txt --limit=6 cache.json
 * Create `cache.json` containing labels inferred for each URL read from stdin. This operation
 * can be run in a loop to fill-up cache.json iteratively.
 *
 * $ wp media label csv --filter=blacklist.txt cache.json < urls.csv
 * Would finally combine the list of <ID, URL> with the JSON file containing the labels and
 * output a consolidated CSV file.
 *
 */
class ImageLabel extends \WP_CLI_Command
{

    /**
     * List already labelled images.
     *
     * ## OPTIONS
     *
     * [--limit=<limit>]
     * : How many entries to dump
     * ---
     * default: 30
     *
     */
    public function dump($args, $assoc_args)
    {
        Tagger::dump_labels($assoc_args['limit']);
    }

    private static function prefix($prefix, $args)
    {
        return $prefix ? array_map(function ($e) use ($prefix) {
            return $prefix . $e;
        }, $args) : $args;
    }

    private static function get_posts_to_tags(bool $missing_alt)
    {
        global $wpdb;
        $only_alt_str = $missing_alt ? ' AND (m2.meta_value IS NULL OR m2.meta_value = "")' : '';

        return $wpdb->get_results(
            $wpdb->prepare(
                "SELECT p.ID as id, p.guid as url, COALESCE(m2.meta_value, '') as alt FROM {$wpdb->posts} p"
                . ' INNER JOIN wp_postmeta m ON p.ID = m.post_id AND m.meta_key = "_wp_attachment_metadata"'
                . ' LEFT JOIN wp_postmeta m2 ON p.ID = m2.post_id AND m2.meta_key = "_wp_attachment_image_alt"'
                . " WHERE p.post_type = %s AND p.guid NOT REGEXP %s {$only_alt_str}",
                'attachment',
                '(mp4|webm|mov)$'
            ),
            ARRAY_A
        );
    }

    /**
     * Label a set of publicly accessible images.
     * An handy way to test the Vision API on any web-file.
     *
     * ## OPTIONS
     *
     * <url>...
     * : URL to label
     *
     * [--prefix=<value>]
     * : Prepend a string (eg: a hostname) to each argument.
     * ---
     * default: ''
     *
     * ## EXAMPLES
     *
     * wp media labels detect --prefix="https://my-domain.com/" 2016/09/14232450.jpg 2016/09/foobar.jpg
     * wp media labels detect https://my-domain.com/2016/09/14232450.jpg https://my-domain.com/2016/09/foobar.jpg
     *
     */
    public function detect($args, $assoc_args)
    {
        $args = self::prefix($assoc_args['prefix'], $args);
        $ret = Tagger::google_detect_label($args);
        print_r($ret);
    }

    /**
     * Get the list of image medias and their labels from the database.
     *
     * ## OPTIONS
     *
     * [--missing-alt]
     * : Only list media missing an image alt.
     *
     * [--root=<baseurl>]
     * : A basename or URL used for guid uniformization and cleanup.
     * ---
     * default: https://example.com/app/
     *
     * ## EXAMPLE
     *
     * This is an equivalent of running:
     * `wp sql query 'SELECT CONCAT(p.ID, "|", p.guid, "|", COALESCE(m2.meta_value, "")) FROM wp_posts p INNER JOIN wp_postmeta m ON p.ID = m.post_id AND m.meta_key = "_wp_attachment_metadata" LEFT JOIN wp_postmeta m2 ON p.ID = m2.post_id AND m2.meta_key = "_wp_attachment_image_alt" WHERE p.post_type = "attachment" AND p.guid NOT REGEXP "(mp4|webm|mov)$"'`
     *
     * ---
     *
     */
    public function candidates($args, $assoc_args)
    {
        $rows = self::get_posts_to_tags($assoc_args['missing-alt']);

        // sed -ri -e '1s/.*/id\|url\|labels/' \
        //         -e '1!s!\|(https?://[^/]+)?(/wp-content)?(/web)?(/app)?/*!\|https://$HOSTNAME/app/!'
        if ($assoc_args['root'] ?? false) {
            $rows = array_map(function ($e) use ($assoc_args) {
                $e['url'] = preg_replace(
                    '!^(?:https?://[^/]+)?(?:/wp-content)?(?:/web)?(?:/app)?/*!',
                    $assoc_args['root'],
                    $e['url']
                );
                return $e;
            }, $rows);
        }
        $out = fopen('php://output', 'w');
        fputcsv($out, array_keys($rows[0]), "\t");
        foreach ($rows as $row) {
            fputcsv($out, $row, "\t");
        }
    }

    /**
     * List already labelled images.
     *
     * ## OPTIONS
     *
     * [--check]
     * : Check a remote file actual existence before submiting.
     *
     * [--limit=<limit>]
     * : How many images to submit in the batch job. Google limit is 16
     * but passing more than 10 often leads to timeouts (error4/error14).
     * ---
     * default: 6
     *
     * <cache_file>
     * : The JSON file keyed by URL used to look for existing label (to resume a run) and
     * and where labels of new files will be stored to.
     * ---
     *
     */
    public function fetch($args, $assoc_args)
    {
        $in = fopen("php://stdin", "r");
        $cache_file = $args[0];
        $cache = file_exists($cache_file) ? json_decode(file_get_contents($cache_file), true) : [];

        $queued = [];
        while (($url = fgets($in)) !== false) {
            $url = trim($url);
            if (! parse_url($url, PHP_URL_SCHEME)) {
                continue;
            }
            if ($assoc_args['check'] ?? false) {
                $exists = file_get_contents($url);
                if (!$exists) {
                    // Keep track that this one is not available so we don't try to
                    // tag it
                    fprintf(STDERR, "404 $url\n");
                }
                fprintf(STDERR, "[check] %s $url\n", $exists ? 'OK ' : '404');
                if (!$exists) {
                    continue;
                }
            }

            if (!isset($cache[$url])) {
                $queued[$url] = 1;
                if (count($queued) >= $assoc_args['limit']) {
                    break;
                }
            }
        }

        if ($cache) {
            fprintf(STDERR, "Existing cache holds %d items\n", count($cache));
        }
        fprintf(STDERR, "Labelling the following %d items:\n%s...\n", count($queued), implode(PHP_EOL, array_keys($queued)));

        if (count($queued)) {
            $ret = Tagger::google_detect_label(array_keys($queued));
            $ret = array_filter($ret, function ($e) {
                return !in_array($e[0] ?? true, ['error14', 'error4'], true);
            });
            fprintf(STDERR, "Adding %d labelled resources\n", count($ret));
            file_put_contents($cache_file, json_encode(array_merge($cache, $ret)));
        }
    }


    /**
     * Output of combined list from a <id, url> CSV file ("|" as a separator) and the JSON file of the labels
     * as created by `wp media label fetch`
     *
     * ## OPTIONS
     *
     * [--filter=<file>]
     * : A file containing labels to exclude (or to remap for lines following the "sub:<search>:<replace>" pattern).
     *
     * <cache_file>
     * : The JSON of existing labels as generated by `wp media label fetch`
     * ---
     *
     */
    public function csv($args, $assoc_args)
    {
        $in = fopen("php://stdin", "r");
        $out = fopen('php://output', 'w');
        $cache = json_decode(file_get_contents($args[0]), true);

        $read_header = $write_header = false;
        while (($data = fgetcsv($in, 1000, "|")) !== false) {
            if (! $read_header) {
                $read_header = $data;
                continue;
            }

            [$id, $url, $labels] = $data;
            if ($labels) {
                continue;
            }

            $orig_labels = $cache[$url] ?? [];
            $new_labels = $this->filterout(array_keys($orig_labels), $assoc_args['filter']);
            if (!$new_labels) {
                fprintf(STDERR, "No labels left (%d / %s)\n", $id, $url);
                fprintf(STDERR, print_r($orig_labels, true));
                continue;
            }

            if (!$write_header) {
                fputcsv($out, $read_header, "\t");
                $write_header = true;
            }

            fputcsv($out, [$id, $url, implode(',', $new_labels)], "\t", chr(0));
        }
    }


    /**
     * Alter posts' "alt" based on a two-columns CSV file containing <ID> and <label>.
     * The post <ID> will see it's meta "_wp_attachment_image_alt" replaced by <label>.
     *
     * ## OPTIONS
     *
     * [--separator=<separator>]
     * : CSV field separator character.
     * ---
     * default: "	"
     *
     * [--limit=<limit>]
     * : How many labels to load at most.
     *
     * [--dry-run]
     * : Just show what would happen.
     *
     * [--verbose]
     * : Just post updates as they happen.
     *
     * <csv_file>
     * : The CSV of <ID, label> to modify media metadata in batch.
     * ---
     *
     */
    public function alt_from_csv($args, $assoc_args)
    {
        $dry_run = $assoc_args['dry-run'] ?? false;
        $verbose = $assoc_args['verbose'] ?? false;
        $limit = intval($assoc_args['limit'] ?? 0);

        $in = fopen($args[0], "r");
        $csv = [];
        $read_header = false;

        while (($data = fgetcsv($in, 1000, $assoc_args['separator'])) !== false) {
            if (! $read_header) {
                $read_header = true;
                if (!is_numeric($data[0])) {
                    // Probably a header
                    continue;
                }
            }

            [$id, $labels] = $data;
            if (!$labels) {
                fprintf(STDERR, "% 3d no label\n", $id);
                continue;
            }

            if ($limit > 0) {
                $labels = implode(',', array_slice(explode(',', $labels), 0, $limit));
            }

            $csv[intval($id)] = $labels;
        }


        $post_missing_tags = self::get_posts_to_tags(true);
        $post_ids_missing_tags = array_flip(
            array_values(
                array_map(
                    function ($e) {
                        return intval($e['id']);
                    },
                    $post_missing_tags
                )
            )
        );

        // $mismatches = array_diff_key($csv, $post_ids_missing_tags);
        $count = 0;
        ksort($csv, SORT_NUMERIC);

        foreach ($csv as $id => $labels) {
            if (! isset($post_ids_missing_tags[$id])) {
                fprintf(STDERR, "% 3d Post has labels or has been deleted\n", $id);
                continue;
            }
            if (! $dry_run) {
                update_post_meta($id, "aitags", true);
                update_post_meta($id, "_wp_attachment_image_alt", $labels);
            }
            if ($dry_run || $verbose) {
                printf("% 3d update using \"%s\"\n", $id, $labels);
            }
            $count++;
        }

        printf("%s %d posts meta\n", $dry_run ? 'Would update' : 'Updated', $count);
        // var_dump($d);die;
    }

    /**
     * labels removal / substitution.
     */
    private function filterout($labels, $filter_file)
    {
        static $blacklist = false;
        static $replacelist = false;

        if (! $blacklist && file_exists($filter_file) ?? false) {
            $replacelist = [];
            $list = array_filter(explode("\n", file_get_contents($filter_file)));
            $blacklist = array_map(
                'strtolower',
                array_filter($list, function ($e) {
                    return strpos($e, 'sub:') === false && strpos($e, '#') !== 0;
                })
            );
            array_walk($list, function ($e) use (&$replacelist) {
                if (strpos($e, 'sub:') === 0 && strpos($e, '#') !== 0) {
                    $x = explode(':', $e);
                    $replacelist[$x[1]] = $x[2];
                }
            });
        }

        $labels = str_ireplace(
            array_keys($replacelist),
            array_values($replacelist),
            $labels
        );

        foreach ($labels as $k => $v) {
            if (in_array(strtolower($v), $blacklist, true)) {
                unset($labels[$k]);
            }
        }

        return array_filter($labels);
    }

    /**
     * Submit and image to the Google "Vision" API, retrieves labels and store them as metadata for a given WP image.
     *
     * ## OPTIONS
     *
     * [--time=<time>]
     * : The strtotime string to pass to WP_Query
     * ---
     * default: "7 months ago"
     *
     * [--verbose]
     * : Be verbose
     * ---
     * default: true
     *
     * [--dry-run]
     * : Don't actually send the images URL to Vision API
     * ---
     * default: false
     *
     * [--check]
     * : Check remote file actual existence before passing its URL to the Vision API.
     * ---
     * default: true
     *
     * ## EXAMPLES
     *
     * wp media labels add --verbose --dry-run --no-check --time="2 month ago"
     *
     */
    public function add($args, $assoc_args)
    {
        $assoc_args += ['check' => true, 'verbose' => true, 'dry-run' => true];
        Tagger::tag_attachments($assoc_args['time'], $assoc_args);
    }

    /**
     * Prints alerts about strangely-labelled image.
     *
     * ## OPTIONS
     *
     * [--hours=<hours>]
     * : Consider the last X hours to consider.
     * ---
     * default: 50
     *
     * [--email]
     * : Whether an email should be send.
     *
     */
    public function alerts($args, $assoc_args)
    {
        // Example callback to check image match food/vg food
        $has_strange_label = function ($labels) {
            if (! isset($labels['food']) && ! isset($labels['vegetarian food'])) {
                return true;
            }
            return false;
        };

        // Example callback to retrieve the resource corresponding to an image
        $comment_retrieval_callback = function ($attachment_id) {
            $comments = get_comments(['meta_query' => [
                'relation' => 'OR',
                ['key' => 'com_illu_repeater_0_com_illu', 'value' => (int)$attachment_id ],
                ['key' => 'com_illu_repeater_1_com_illu', 'value' => (int)$attachment_id ],
                ['key' => 'com_illu_repeater_2_com_illu', 'value' => (int)$attachment_id ]]]);
            return array_map(function ($e) {
                return admin_url('comment.php?action=editcomment&c=') . $e->comment_ID;
            }, $comments);
        };

        Tagger::alert_strange_labels(
            (int)$assoc_args['hours'] * HOUR_IN_SECONDS,
            $has_strange_label,
            ['email' => isset($assoc_args['email']),
             'verbose' => true,
             'parent_resource_callback' => $comment_retrieval_callback]
        );
    }
}
