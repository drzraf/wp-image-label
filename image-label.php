<?php

/**
 * Plugin Name: Image labeller
 * Plugin URI: https://gitlab.com/drzraf/wp-image-label
 * Description: WP CLI command to add labels to media image files using the Google Vision API.
 * Version: 0.1.0
 * Author: Raphaël Droz
 * Author URI: https://gitlab.com/drzraf
 * License: GPL-3
 * TextDomain:
 * DomainPath:
 * Network:
 *
 * @package drzraf/wp-image-label
 */

if (! class_exists('\WP_CLI')) {
    return;
}

$wpcli_image_label_autoloader = __DIR__ . '/vendor/autoload.php';

if (file_exists($wpcli_image_label_autoloader)) {
    require_once $wpcli_image_label_autoloader;
}

\WP_CLI::add_command('media label', \WP_CLI\WpImageLabel\ImageLabel::class);
